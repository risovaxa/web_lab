import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';

import '../index.css';

function CarouselItem(props) {
  return (
    <div className = "carousel-item__rapper">
      <img className="kortinka" src={props.source} alt="" />
      <div className = "carousel-item__content">
        <h3 className = "carousel-item__xeder">{props.name}</h3>
        <h3 className = "carousel-item__fyter">{props.activity}</h3>
      </div>
    </div>
  );
}

class MyCarousel extends Component {

  render() {
    const responsive = {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1024: {
        items: 3
      }
    };
    return (
      <AliceCarousel dotsDisabled={true} buttonsDisabled={true} mouseDragEnabled responsive={responsive}>
        <CarouselItem  className = "" source="/img/layer7.jpg" name="T-MOBILE" activity="Telecom" />
        <CarouselItem source="/img/layer7.jpg" name="T-MOBILE" activity="Telecom" />
        <CarouselItem source="/img/layer7.jpg" name="T-MOBILE" activity="Telecom" />
        <CarouselItem source="/img/layer7.jpg" name="T-MOBILE" activity="Telecom" />
        <CarouselItem source="/img/layer7.jpg" name="T-MOBILE" activity="Telecom" />
      </AliceCarousel>
    );
  }
}
export default MyCarousel;