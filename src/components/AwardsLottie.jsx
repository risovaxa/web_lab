import React, { Component } from 'react'
import Lottie from 'react-lottie'


class AwardsLottie extends Component {
  render(){

    const defaultOptions = {
      loop: true,
      autoplay: true, 
      animationData: require("./runner.json"),
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    return(
        <Lottie options={defaultOptions}
              height={400}
              width={400}
        />
    )
  }
}
export default AwardsLottie