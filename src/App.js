import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
// import Tabs from "./components/Tabs";

import glamorous from "glamorous";

import icon_support from "./img/icon_support.png"
import icon_bulk_sms from "./img/icon_bulk_sms.png"
import icon_money_exchange from "./img/icon_money_exchange.png"
import icon_business_boosting from "./img/icon_business_boosting.png"

import CustomerLottie from './components/CustomerLottie';
import AwardsLottie from './components/AwardsLottie';
import MyCarousel from './components/MyCarousel';

import 'react-tabs/style/react-tabs.css';
import './index.css';
import 'react-alice-carousel/lib/alice-carousel.css';



function FirstSection(props) {
  return (
    <section className="section__left-item first-section">
      <div className="wrapper left-item">
        <div className="main-title">
          <h1>CONSULT. WIN. CYGWIN.</h1>
          <a className="main-button" href="/">Our Services</a>
        </div>
      </div>
    </section>
  );
}
function InfoSection(props) {
  return (
    <section className="section__center-item">
      <div className="wrapper center-item">
        <img className="descr__img" src={require("./img/layer9.png")} alt="" />
        <div className="descr-block__wrapper">
          <h2>Consulting aid</h2>
          <p className="descr__main-text">Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
            the industry's standard
            dummy text ever since the 1500s, when an unknown.</p>
          <p className="descr__secondary-text">Passage of Lorem Ipsum, you need to be sure there isn't anything
            embarrassing hidden in the middle of
            text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
          <a className="main-button" href="/">Read More</a>
        </div>
      </div>
    </section>
  );
}
function IndustriesSection(props) {
  return (
    <section className="section__left-item second-section">
      <div className="wrapper left-item">
        <div className="carousel-item">
          <h1>industries</h1>
          <div className="carousel__wrapper">
            <div className="slide">

              <SlideItem
                path={icon_support}
                item_name="Customer Support"
                item_content="All the Lorem Ipsum on the Internet tend to repeat predefined chunks necessary generators..."
              />
              <SlideItem
                path={icon_money_exchange}
                item_name="Customer Support"
                item_content="All the Lorem Ipsum on the Internet tend to repeat predefined chunks necessary generators..."
              />
              <SlideItem
                path={icon_bulk_sms}
                item_name="Customer Support"
                item_content="All the Lorem Ipsum on the Internet tend to repeat predefined chunks necessary generators..."
              />
              <SlideItem
                path={icon_business_boosting}
                item_name="Customer Support"
                item_content="All the Lorem Ipsum on the Internet tend to repeat predefined chunks necessary generators..."
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
function SlideItem(props) {
  return (
    <div className="slide-item">
      <img src={props.path} alt="" />
      <div className="slide-item__content-wrapper">
        <h3>{props.item_name} </h3>
        <p>{props.item_content}</p>
      </div>
    </div>
  );

}

function Carousell(props) {
  return (
      <MyCarousel/>
  );
}

// function CarouselItem(props) {
//   return (
//     <a className="carousel-item__wrapper" href="/">
//       <div className="carousel-item">
//         <img className="carousel-item__img" src={props.source} alt="" />
//         <div className="carousel-item__footer">
//           <div className="carousel-item__text">
//             <h3>{props.name}</h3>
//             <span>{props.activity}</span>
//           </div>
//         </div>
//       </div>
//     </a>
//   );
// }
function AwardsSection(props) {
  return (
    <section className="section__center-item awards">
      <div className="wrapper center-item">
        <Tabs>
          <TabList>
            <Tab>
              <AwardsElement
                source="/img/icon_happy_clients.png"
                value="2500+"
                description="happy clients"
              />
            </Tab>
            <Tab>
              <AwardsElement
                source="/img/icon_awards.png"
                value="38"
                description="awards"
              />
            </Tab>
            <Tab disabled>
              <AwardsElement
                source="/img/icon_employs.png"
                value="390+"
                description="employees"
              />
            </Tab>
          </TabList>
          <TabPanel><AwardsLottie/></TabPanel>
          <TabPanel><CustomerLottie/></TabPanel>
        </Tabs>
      </div>
    </section>
  );
}
function AwardsElement(props) {
  return (
    <div className="awards__item">
      <img src={props.source} alt="" />
      <div className="awards__content">
        <span className="awards__value">
          {props.value}
        </span>
        <span className="awards__descr">
          {props.description}
        </span>
      </div>
    </div>
  );
}
function SummarySection(props) {
  return (
    <section className="section__center-item map">
      <div className="wrapper center-item">
        <h2>Why you waiting start a New Projects ?</h2>
        <a className="main-button" href="/">Start project now</a>
      </div>
    </section>
  );
}

function HomeView(props) {
  return (
    <div>
      <FirstSection />
      <InfoSection />
      <IndustriesSection />
      <Carousell />
      <AwardsSection />
      <SummarySection />
    </div>
  );
}
function AboutView(props) {
  return (
    <div>

    </div>
  );
}

function Window(props) {
  return (
    <div>
      <div className="wrapper header">
        <div className="header__navigation">
          <ul>
            <li>home</li>
            <li>about</li>
            <li>price</li>
            <li>faq</li>
          </ul>
        </div>
      </div>
      <div className="main">
        {props.view}
      </div>
    </div>
  );
}

class App extends Component {
  render() {
    return (<Window view={<HomeView />} />);
  }
}
export default App;


